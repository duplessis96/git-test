package Account;

public class SavingsAccount extends Account {

    public SavingsAccount(String name, double balance) {

        super(name, balance);

    }

    public SavingsAccount(){

        super();
    }

    @Override
    public void addInterest() {

        setBalance(getBalance() * 1.4);


    }
    
}