package Account;

public class TestInheritance { 

    public static void main(String[] args) {
        
        Account[] acc_arr = new Account[3];
        acc_arr[0] = new SavingsAccount();
        acc_arr[1] = new SavingsAccount();
        acc_arr[2] = new CurrentAccount();
        
        acc_arr[0].setBalance(2); 
        acc_arr[1].setBalance(4);
        acc_arr[2].setBalance(6);

        for(Account a: acc_arr) {

         //   System.out.printf("Name: %s\nBalance: %d" a.getName(), a.getBalance());

            System.out.println("Name: " + a.getName() + "\nBalance: " + a.getBalance());

        }

    }
    
}