package Account;

public abstract class Account {

    private double balance;
    private static double interestRate;
    private String name;

    public Account(String s, double d) {

        name = s;
        balance = d;

    }

    public Account() {

        this("Donavan", 50);

    }

    double getBalance() {return balance;}
    String getName() {return name;}
    void setBalance(double balance) {this.balance = balance;}
    void setName(String name) {this.name = name;}
    static void setInterestRate(double interestRate) {Account.interestRate = interestRate;}
    static double getInterestRate() {return interestRate;}

    public abstract void addInterest();

    public boolean withdraw(double amount) {

        if(amount <= balance) {

           System.out.println("Successful withdraw! ");
           balance -= amount;
           return true; 
        }

        System.out.println("Error, Not enough money!");
        return false;

    }

    public boolean withdraw() {

        int arb_amount = 20;

        if(withdraw(arb_amount) == true) {

            return true;

        }

        return false;

    }


}