package Account;

public class TestAccount {

    public static void main(String[] args) {
        
        Account myAccount = new SavingsAccount();
        myAccount.setBalance(50.0);
        myAccount.setName("Donavan Duplessis");

        System.out.println("My name is " + myAccount.getName() + " and my balance is: " + myAccount.getBalance());

        myAccount.addInterest();
        System.out.println("Adding interests gives us: " + myAccount.getBalance());

    }

}